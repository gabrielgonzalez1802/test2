{
  "contact_email" : "info@latinon.com",
  "contact_address" : "304 Indian Trace #713, Ft Lauderdale, FL 33326",
  "version" : "1.0",
  "identifiers" : [ {
    "name" : "DUNS",
    "value" : "081289926"
  }, {
    "name" : "TAG-ID",
    "value" : "c5ec92f61714fa6b"
  } ],
  "sellers" : [ {
    "seller_id" : 2,
    "name" : "Bloque de armas",
    "domain" : "bloquedearmas.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 7,
    "name" : "DIARIO LA VERDAD, C.A.",
    "domain" : "laverdad.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 23,
    "name" : "Diario El Universal",
    "domain" : "eluniversal.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 25,
    "name" : "CODIGO DIGITAL, S.A.",
    "domain" : "soy502.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 31,
    "name" : "Casa Editora Prensa Libre S.A",
    "domain" : "prensalibre.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 32,
    "name" : "El Heraldo S.A.",
    "domain" : "elheraldo.co",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 34,
    "name" : "EL COLOMBIANO S.A. Y CIA SCA",
    "domain" : "elcolombiano.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 39,
    "name" : "El Pais S.A.",
    "domain" : "elpais.com.co",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 40,
    "name" : "Editorial Notitarde, C.A.",
    "domain" : "notitarde.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 41,
    "name" : "Grupo Diario Libre",
    "domain" : "diariolibre.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 42,
    "name" : "Multimedios del Caribe",
    "domain" : "elcaribe.com.do",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 49,
    "name" : "SOCIEDAD TELEVISION DE ANTIOQUIA LTDA",
    "domain" : "teleantioquia.co",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 55,
    "name" : "El Cronista Comercial S.A.",
    "domain" : "cronista.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 75,
    "name" : "Medios Ediasa ",
    "domain" : "eldiario.ec",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 104,
    "name" : "NEFIR SA",
    "domain" : "ambito.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 171,
    "name" : "Grupo Imagen",
    "domain" : "imagen.com.mx",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 209,
    "name" : "LALUPA",
    "domain" : "lalupa.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 250,
    "name" : "VENEZUELATUYA.COM S.A.",
    "domain" : "venezuelatuya.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 254,
    "name" : "DIARIO CONTRASTE",
    "domain" : "diariocontraste.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 260,
    "name" : "Editora del Mar S.A.",
    "domain" : "eluniversal.com.co",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 263,
    "name" : "Grupo La Republica Publicaciones",
    "domain" : "larepublica.pe",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 275,
    "name" : "TELEVISA S.A. de C.V.",
    "domain" : "televisa.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 276,
    "name" : "STG Consulting",
    "domain" : "dolartoday.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 289,
    "name" : "Graficos Nacionales S.A.",
    "domain" : "graficosnacionales.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 317,
    "name" : "1050 SOLUTION INC",
    "domain" : "musica.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 318,
    "name" : "EDITORA NUEVO AMANECER S.A.",
    "domain" : "elnuevodiario.com.ni",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 319,
    "name" : "FUTBOLECUADOR.COM",
    "domain" : "futbolecuador.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 322,
    "name" : "GRANOS DIGITALES 2016, C.A.",
    "domain" : "caraotadigital.net",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 345,
    "name" : "El Financiero Marketing SA",
    "domain" : "elfinanciero.com.mx",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 371,
    "name" : "Minuto 30 S.A.S.",
    "domain" : "minuto30.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 389,
    "name" : "Television del Pacifico, S.A. de C.V.",
    "domain" : "tvpacifico.mx",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 399,
    "name" : "REPUBLICA.GT",
    "domain" : "republica.gt",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 403,
    "name" : "Grupo Noticias Voz e Imagen de Oaxaca",
    "domain" : "nvinoticias.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 410,
    "name" : "EL FARANDI",
    "domain" : "elfarandi.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 420,
    "name" : "Noticiero Digital",
    "domain" : "noticierodigital.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 465,
    "name" : "AX MULTIMEDIA",
    "domain" : "lapagina.com.sv",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 519,
    "name" : "TELEAMAZONAS GUAYAQUIL S.A",
    "domain" : "teleamazonas.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 545,
    "name" : "VERIFIKADO LLC",
    "domain" : "verifikado.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 567,
    "name" : "EMPRESA DE COMUNICACION SOCIAL EL DEBER SA",
    "domain" : "eldeber.com.bo",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 578,
    "name" : "Emisoras Unidas",
    "domain" : "emisorasunidas.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 582,
    "name" : "La Opinion S.A.",
    "domain" : "laopinion.com.co",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 602,
    "name" : "Grupo Clarin",
    "domain" : "grupoclarin.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 630,
    "name" : "GRUPO PERISCOPIO",
    "domain" : "elperiscopio.cl",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 639,
    "name" : "100% Noticias",
    "domain" : "100noticias.com.ni",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 640,
    "name" : "GRUPO MUNDO MULTIMEDIA",
    "domain" : "elmundo.sv",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 642,
    "name" : "LAIGUANA.TV",
    "domain" : "laiguana.tv",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 646,
    "name" : "GRUPO 21",
    "domain" : "g21.com.mx",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 651,
    "name" : "Latina",
    "domain" : "latina.pe",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 665,
    "name" : "Grupo Copesa",
    "domain" : "grupocopesa.cl",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 678,
    "name" : "ASOCIACION CIVIL COFEIN",
    "domain" : "elpitazo.net",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 679,
    "name" : "EL COOPERANTE",
    "domain" : "elcooperante.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 682,
    "name" : "TALCUAL",
    "domain" : "talcualdigital.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 685,
    "name" : "Grupo REFORMA",
    "domain" : "reforma.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 695,
    "name" : "GRUPO EDITORIAL SIN",
    "domain" : "noticiassin.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 696,
    "name" : "EDITORES ORIENTALES C.A.",
    "domain" : "eltiempo.com.ve",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 709,
    "name" : "DIARIO EL IMPULSO",
    "domain" : "elimpulso.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 715,
    "name" : "Arte Radiotelevisivo Argentino S.A",
    "domain" : "artear.com.ar",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 718,
    "name" : "EDITORIAL CARACTERES MX",
    "domain" : "caracteres.mx",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 722,
    "name" : "MIAMI NEWS 24 INC",
    "domain" : "miaminews24.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 725,
    "name" : "Comercializadora de Medios del Interior S.A.",
    "domain" : "cmi.com.ar",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 744,
    "name" : "LA NOTA LATINA",
    "domain" : "lanota-latina.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2004,
    "name" : "Ideas producciones CA",
    "domain" : "noticialdia.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2007,
    "name" : "CORPORACION LA PRENSA, S.A.",
    "domain" : "corprensa.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2010,
    "name" : "SU NOTICIERO",
    "domain" : "sunoticiero.net",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2011,
    "name" : "Corporacion Medcom Panama S.A.",
    "domain" : "medcom.com.pa",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2086,
    "name" : "TVN Media",
    "domain" : "tvn-2.com",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2087,
    "name" : "Publinews Guatemala",
    "domain" : "publinews.gt",
    "seller_type" : "PUBLISHER"
  }, {
    "seller_id" : 2091,
    "name" : "NYR Co",
    "domain" : "nyrcompany.com",
    "seller_type" : "PUBLISHER"
  } ]
}
